<%@ page contentType="text/html; charset=utf-8" language="java" pageEncoding="utf-8"%>
<jsp:include page="./header.jsp"></jsp:include>
<style>
.pagination{
    display : block !important;
    margin-right:10px;
}
.pagination-detail{
    margin-left:10px;
}
thead {
    background-color: #6ab6e1;
    
}
.th-inner {
    color : white;
}
</style>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-6" style="width: 100%;">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools" style="left:15px;top:15px;">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="请输入手机号码" style="width:180px;">

                  <div class="input-group-btn">
                    <button id="searchBtn" type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="userInfoTab">
              </table>
            </div>
            <!-- /.box-body -->
          </div>

          </div>
          <!-- /.box -->
          </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../js/table.js"></script>
 <jsp:include page="./footer.jsp"></jsp:include>