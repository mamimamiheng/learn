<%@ page contentType="text/html; charset=utf-8" language="java" pageEncoding="utf-8"%>
<jsp:include page="./header.jsp"></jsp:include>
<style>
.pagination{
    display : block !important;
    margin-right:10px;
}
.pagination-detail{
    margin-left:10px;
}
thead {
    background-color: #6ab6e1;
    
}
.th-inner {
    color : white;
}
</style>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-6" style="width: 100%;">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools" style="left:15px;top:15px;">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="请输入手机号码" style="width:180px;">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="dishInfoTab">
                            <!-- <table id="userInfoTab" class="table table-hover"> -->
              
                <!-- <tr>
                  <th>用户ID</th>
                  <th>手机号码</th>
                  <th>注册时间</th>
                  <th>上次登录时间</th>
                  <th>操作</th>
                </tr>
                <tr>
                  <td>0001</td>
                  <td>15211111111</td>
                  <td>2019-02-22 12:23:00</td>
                  <td>2019-02-22 12:23:00</td>
                  <td>说明文字</td>
                </tr>
                <tr>
                  <td>0001</td>
                  <td>15211111111</td>
                  <td>2019-02-22 12:23:00</td>
                  <td>2019-02-22 12:23:00</td>
                  <td>说明文字</td>
                </tr>
                <tr>
                  <td>0001</td>
                  <td>15211111111</td>
                  <td>2019-02-22 12:23:00</td>
                  <td>2019-02-22 12:23:00</td>
                  <td>说明文字</td>
                </tr>
                <tr>
                  <td>0001</td>
                  <td>15211111111</td>
                  <td>2019-02-22 12:23:00</td>
                  <td>2019-02-22 12:23:00</td>
                  <td>说明文字</td>
                </tr> -->
                
              </table>
            </div>
            <!-- /.box-body -->
            <!-- <div id="#toolbar" class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div> -->
          </div>

          </div>
          <!-- /.box -->
          </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../js/dish.js"></script>
 <jsp:include page="./footer.jsp"></jsp:include>