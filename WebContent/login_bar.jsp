<%@ page contentType="text/html; charset=utf-8" language="java" pageEncoding="utf-8"%>

<div class="navbar-custom-menu">
	<ul class="nav navbar-nav">
		<li class="dropdown user user-menu user-menu_dis" style="display:block;" data-toggle="modal" data-target="#modal-login">
		  <a href="#" class="dropdown-toggle">
			<span class="hidden-xs">登录</span>
		  </a>
		</li>
		<li class="dropdown user user-menu user-menu_dis" style="display:block;" data-toggle="modal" data-target="#modal-regist">
		  <a href="#" class="dropdown-toggle">
			<span class="hidden-xs">注册</span>
		  </a>
		</li> 
		<li class="dropdown user user-menu user-menu_hid" style="display:none;">
		    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
		      <img src="./dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
		      <span id="userNumber" class="hidden-xs">Alexander Pierce</span>
		    </a>
			<ul class="dropdown-menu">
				<li class="user-header"><img src="./dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
					<p>
						Alexander Pierce - Web Developer 
						<small>Member since Nov.2012</small>
					</p>
				</li>
				<li class="user-body">
					<div class="row">
						<div class="col-xs-4 text-center">
							<a href="#">Followers</a>
						</div>
						<div class="col-xs-4 text-center">
							<a href="#">Sales</a>
						</div>
						<div class="col-xs-4 text-center">
							<a href="#">Friends</a>
						</div>
					</div>
				</li>
				<li class="user-footer">
					<div class="pull-left">
						<a href="#" class="btn btn-default btn-flat">Profile</a>
					</div>
					<div class="pull-right">
						<a href="#" class="btn btn-default btn-flat">Sign out</a>
					</div>
				</li>
			</ul>
		</li>

	</ul>
</div>

<!------------------------------------------- 样式覆盖 ------------------------------------------->		
<style>

.modal-backdrop{
z-index:900;

}
<!------------------------------------------- 样式覆盖 ------------------------------------------->		
</style>

<!------------------------------------------- 登录 ------------------------------------------->
<div class="modal fade" id="modal-login" style="z-index:999;">
	<div class="modal-dialog" style="width:480px;">
		<div class="modal-content">
			<div class="modal-header">
				<button id="loginCloseBtn" type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">登录</h4>
			</div>
			<div class="modal-body">
				<div class="login-box">
				  <div class="login-box-body">
				      <div class="form-group has-feedback">
				        <input name="servNumber1" type="phone" class="form-control" placeholder="手机号码">
				        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
				      </div>
				      <div class="form-group input-group">
				        <input name="validateCode1" type="text" class="form-control">
				        <a id="getValidateCode1" class="input-group-addon" href="#" style="background-color:#00c0ef;"><i class="fa" style="color:white;">发送验证码</i></a>
				      </div>
				      <input name="userType1" value="U" type="hidden"/>
				      <div class="row">
				        <div class="col-xs-12">
				          <button id="loginBtn" type="button" class="btn btn-primary btn-block btn-flat">登录</button>
				        </div>
				      </div>
				
				    
				  </div>
				  <div id="validateCodeDiv1" class="alert alert-info alert-dismissible" style="margin-top:40px;display:none;">
				    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				    <h4><i class="fa"></i> 手机验证码</h4>
				    <h4 id="validateCode1">123456</h4>
				  </div>
				  <!-- /.login-box-body -->
				</div>
				<!-- /.login-box -->
			</div>
		</div>
	</div>
</div>


<!------------------------------------------- 注册 ------------------------------------------->
<div class="modal fade" id="modal-regist" style="z-index:999;">
	<div class="modal-dialog" style="width:480px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">注册</h4>
			</div>
			<div class="modal-body">
				<div class="login-box">
				  <div class="login-box-body">
				    <form action="./index.jsp" method="post">
				      <div class="form-group has-feedback">
				        <input name="servNumber" type="phone" class="form-control" placeholder="手机号码">
				        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
				      </div>
				      <div class="form-group input-group">
				        <input name="validateCode" type="text" class="form-control">
				        <a id="getValidateCode2" class="input-group-addon" href="#" style="background-color:#00c0ef;"><i class="fa" style="color:white;">发送验证码</i></a>
				      </div>
				      <div class="form-group has-feedback">
				        <input name="address" type="phone" class="form-control" placeholder="收货地址">
				        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
				      </div>
				      <input name="userType" value="U" type="hidden"/>
				      <div class="row">
				        <!-- <div class="col-xs-8">
				          <div class="checkbox icheck">
				            <label>
				              <input type="checkbox"> 记住密码
				            </label>
				          </div>
				        </div> -->
				        <!-- /.col -->
				        <div class="col-xs-12">
				          <button id="registBtn" type="submit" class="btn btn-primary btn-block btn-flat">登录</button>
				        </div>
				        <!-- /.col -->
				      </div>
				    </form>
				
				    
				  </div>
				  <div id="validateCodeDiv2" class="alert alert-info alert-dismissible" style="margin-top:40px;display:none;">
				    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				    <h4><i class="fa"></i> 手机验证码</h4>
				    <h4 id="validateCode2">123456</h4>
				  </div>
				  <!-- /.login-box-body -->
				</div>
				<!-- /.login-box -->
			</div>
		</div>
	</div>
</div>