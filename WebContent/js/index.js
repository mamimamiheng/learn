/**
 * 前台首页
 */

/************************************获取验证码************************************/	  
  $("#getValidateCode1").click(function(){
		
		var servNumber = $("[name='servNumber1']").val();
		
		if(null == servNumber || "" == servNumber){
			alert("请填写手机号码!");
		}
		$.ajax({
			type : "POST",
			data : JSON.stringify({'servNumber':servNumber}),
			url  : "/learn/userController/getValidateCode.do",
			contentType : 'application/json;charset=UTF-8',
			success : function(data,status){
			    if("success" == status){
			    	$("#validateCodeDiv1").show();
			    	$("#validateCode1").html(data.object);
			    }
		  
			}
		});
	});
/************************************用户登陆************************************/	  
  $("#loginBtn").click(function(){
	  
	  var servNumber = $("[name='servNumber1']").val();
	  var validateCode = $("[name='validateCode1']").val();
	  
	  $.ajax({
          url:"/learn/userController/userLogin.do",
          type:"POST",
		  data:JSON.stringify({'servNumber':servNumber,'validateCode':validateCode}),
		  contentType: "application/json",
		  success:function(data,status){
		      $(".user-menu_dis").css("display","none");
		      $(".user-menu_hid").css("display","block");
		      $(".user-menu_hid").css("display","block");
		      $("#loginCloseBtn").click();
		      $("#userNumber").html(servNumber);
	      },
	      error:function(data){
	    	  alert("未找到相关用户");
	      }
	  })
  });

