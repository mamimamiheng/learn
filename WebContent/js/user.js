$(function(){
	var bootstrapTable = new BootstrapTable();
	bootstrapTable.Init();
	$(".fixed-table-pagination div[class='pagination'] ").css('display','block');
	
	
	$("#searchBtn").click(function(){
		alert();
		bootstrapTable.Destroy();
		bootstrapTable.Init();
	});
});


var BootstrapTable = function () {
	
    var bootstrapTable = new Object();
    
    
    bootstrapTable.Destroy = function () {
    	$('#userInfoTab').bootstrapTable('destroy');
    }
    //初始化Table
    bootstrapTable.Init = function () {
        $('#userInfoTab').bootstrapTable({
            url: '/learn/userController/listUser.do',         //请求后台的URL（*）
            method: 'post',                      //请求方式（*）
            toolbar: '#toolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: true,                   //是否显示分页（*）
            paginationFirstText: "首页",
            paginationPreText: "上一页",
            paginationNextText: "下一页",
            paginationLastText: "末页",
//            sortable: false,                     //是否启用排序
            sortOrder: "asc",                   //排序方式
            queryParams: bootstrapTable.queryParams,//传递参数（*）
            sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber: 1,
            total:1,//初始化加载第一页，默认第一页
            pageSize: 10,                       //每页的记录行数（*）
            pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
//            search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            contentType: "application/json",
//            strictSearch: true,
//            showColumns: true,                  //是否显示所有的列
//            showRefresh: true,                  //是否显示刷新按钮
            minimumCountColumns: 2,             //最少允许的列数
            clickToSelect: true,                //是否启用点击选中行
//            height: 500,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: "no",                     //每一行的唯一标识，一般为主键列
//            showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
//            cardView: false,                    //是否显示详细视图
//            detailView: false,                   //是否显示父子表
            columns: [
            {
            	title: '用户ID',
            	field: 'userId',
            	align: 'center'
            }, {
            	title: '手机号码',
            	field: 'servNumber',
            	align: 'center'
            }, {
            	title: '注册时间',
            	field: 'registerTime',
            	align: 'center'
            },
            {
            	title: '地址',
                field: 'address',
            	align: 'center'
            },
            {
            	title: '操作',
                field: '1',
            	align: 'center'
            }
            ]
        });

    };


    //得到查询的参数
    bootstrapTable.queryParams = function (params) {
        var temp = {   //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
            limit: params.limit,   //页面大小
            offset:params.offset
        };
        return temp;
    };
    return bootstrapTable;
};


function operateFormatter(value, row, index) {//赋予的参数
    return [
        '<a class="btn active disabled" href="#">编辑</a>',
        '<a class="btn active" href="#">档案</a>',
        '<a class="btn btn-default" href="#">记录</a>',
        '<a class="btn active" href="#">准入</a>'
    ].join('');
}