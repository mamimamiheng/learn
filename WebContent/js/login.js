/**
 * 网上订餐系统登录时获取验证码
 */
$(function(){
	$("#validateCodeDiv").hide();
});


$("#getValidateCode").click(function(){
	
	var servNumber = $("[name='servNumber']").val();
	
	if(null == servNumber || "" == servNumber){
		alert("请填写手机号码!");
	}
	
	$.ajax({
		type : "POST",
		data : JSON.stringify({'servNumber':servNumber}),
		url  : "/learn/userController/getValidateCode.do",
		contentType : 'application/json;charset=UTF-8',
		success : function(data,status){
		    if("success" == status){
		    	$("#validateCodeDiv").show();
		    	$("#validateCode").html(data.object);
		    }
	  
		}
	});
});