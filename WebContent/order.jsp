<%@ page contentType="text/html; charset=utf-8" language="java" pageEncoding="utf-8"%>

<div class="modal fade" id="modal-default">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">我的订单</h4>
			</div>
			<div class="modal-body">
				<div class="row" style="height: 80px; margin-bottom: 15px;">
					<div class="col-md-10" style="height: 80px;padding-right:2px;">
						<div class="info-box bg-green" style="height: 80px; background-color: #4ecdec;">
							<span class="info-box-icon" style="height: 80px; width: 80px;">
							    <img src="./img02.jpg" class="img-circle" alt="User Image" style="height: 70px; width: 150px; margin: 5px auto;">
							</span>

							<div class="info-box-content" style="margin-left: 80px;">
								<span class="info-box-text" style="font-weight: 800; font-size: 12px;">鱼香肉丝</span>
								<!-- <span class="info-box-number">41,410</span> -->
								<div class="progress">
									<div class="progress-bar" style="width: 100%"></div>
								</div>
								<span class="progress-description"> 酸甜爽口，唇齿留香 </span>
							</div>
						</div>
					</div>
					<div class="col-md-2" style="height: 80px;padding-left:0;">
						<div class="info-box bg-green" style="height: 80px; background-color: #4ecdec;"></div>
					</div>
				</div>
				
				
				<div class="row" style="height: 80px; margin-bottom: 15px;">
					<div class="col-md-10" style="height: 80px;padding-right:2px;">
						<div class="info-box bg-yellow" style="height: 80px; background-color: #4ecdec;">
							<span class="info-box-icon" style="height: 80px; width: 80px;">
							    <img src="./img02.jpg" class="img-circle" alt="User Image" style="height: 70px; width: 150px; margin: 5px auto;">
							</span>

							<div class="info-box-content" style="margin-left: 80px;">
								<span class="info-box-text" style="font-weight: 800; font-size: 12px;">鱼香肉丝</span>
								<!-- <span class="info-box-number">41,410</span> -->
								<div class="progress">
									<div class="progress-bar" style="width: 100%"></div>
								</div>
								<span class="progress-description"> 酸甜爽口，唇齿留香 </span>
							</div>
						</div>
					</div>
					<div class="col-md-2" style="height: 80px;padding-left:0;">
						<div class="info-box bg-yellow" style="height: 80px; background-color: #4ecdec;"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left"
					data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary">下单</button>
			</div>
		</div>
	</div>
</div>
