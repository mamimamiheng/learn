<%@ page contentType="text/html; charset=utf-8" language="java"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>丫丫外卖</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="./bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="./bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="./bower_components/Ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="./dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="./dist/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
.dish_type {
	height: 7em;
	width: 7em;
}
</style>
<body class="hold-transition skin-blue layout-top-nav">
	<div class="wrapper">
	
		<header class="main-header">
			<nav class="navbar navbar-static-top">
				<div class="container" >
				
					<div class="navbar-header">
						<a href="./index2.html" class="navbar-brand"><b>丫丫外卖</b></a>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>

					<div class="collapse navbar-collapse pull-left"
						id="navbar-collapse" style="margin-left: 200px;">
						<form class="navbar-form navbar-left" role="search">
							<div class="form-group">
								<input type="text" class="form-control" id="navbar-search-input"
									placeholder="找到最适合您的口味" style="width: 340px;">
							</div>
						</form>
					</div>
<!------------------------------------------- 登录按钮 ------------------------------------------->	
<jsp:include page="./login_bar.jsp"></jsp:include>	
<!------------------------------------------- 登录按钮 ------------------------------------------->					
				</div>
			</nav>
		</header>
		
		<div class="content-wrapper">
			<div class="container" style="width:92%;">
				<section class="content">
<!------------------------------------------- 轮播图片 ------------------------------------------->	
<jsp:include page="./banner.jsp"></jsp:include>	
<!------------------------------------------- 轮播图片 ------------------------------------------->						

<!------------------------------------------- 菜品类别 ------------------------------------------->	
<jsp:include page="./dish_type.jsp"></jsp:include>	
<!------------------------------------------- 菜品类别 ------------------------------------------->	

<!------------------------------------------- 菜品展示 ------------------------------------------->	
<jsp:include page="./dish.jsp"></jsp:include>	
<!------------------------------------------- 菜品展示 ------------------------------------------->					
				</section>
			</div>
		</div>
<!------------------------------------------- 订单弹出框 ------------------------------------------->	
<jsp:include page="./order.jsp"></jsp:include>	
<!------------------------------------------- 订单弹出框 ------------------------------------------->		
		
		<footer class="main-footer">
			<div class="container">
				<div class="pull-right hidden-xs">
					<b>Version</b> 2.4.0
				</div>
				<strong>Copyright &copy; 2014-2016 <a href="#">Zhou Shujing</a>.
				</strong> All rights reserved.
			</div>
		</footer>
	</div>

	<!-- jQuery 3 -->
	<script src="./bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="./bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="./bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="./dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="./js/index.js"></script>
</body>
</html>
