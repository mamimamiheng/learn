package com.agent.dao.mapper;

import java.util.List;

import com.agent.pojo.Order;

public interface OrderMapper {
	
	
    int insertOrder(Order Order);
	
	
	int updateOrder(Order Order);
	
	
	Order getOrder(Order Order);
	
	
	List<Order> getOrderList(Order Order);

	
}