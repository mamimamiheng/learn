package com.agent.dao.mapper;

import java.util.List;

import com.agent.pojo.User;

public interface UserMapper {
	
	
	int insertUser(User user);
	
	
	int updateUser(User user);
	
	
	User getUser(User user);
	
	
	List<User> getUserList(User user);
    

}