package com.agent.dao.mapper;

import java.util.List;

import com.agent.pojo.Dish;
import com.agent.pojo.DishRel;

public interface DishRelMapper {
	
    int insertDishRel(List<DishRel> dishRelList);
	
	int updateDishRel(DishRel dishRel);
	
	
	List<DishRel> listDishRel(DishRel dishRel);
}