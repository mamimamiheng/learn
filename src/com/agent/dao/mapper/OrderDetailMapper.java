package com.agent.dao.mapper;

import java.util.List;

import com.agent.pojo.OrderDetail;

public interface OrderDetailMapper {
	
    int insertOrderDetail(List<OrderDetail> orderDetailList);
	
	
	int updateOrderDetail(OrderDetail orderDetail);
	
	
	OrderDetail getOrderDetail(Integer orderDetailId);
	
	
	List<OrderDetail> getOrderDetailList(OrderDetail orderDetail);	
	
   
}