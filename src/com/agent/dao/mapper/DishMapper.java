package com.agent.dao.mapper;

import java.util.List;

import com.agent.pojo.Dish;

public interface DishMapper {
	
    int insertDish(Dish dish);
	
	int updateDish(Dish dish);
	
	Dish getDish(Dish dish);
	
	List<Dish> listDish(Dish dish);
	
	int countDish(Dish dish);
	
}