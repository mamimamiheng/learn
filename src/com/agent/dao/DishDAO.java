package com.agent.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agent.dao.mapper.DishMapper;
import com.agent.dao.mapper.DishRelMapper;
import com.agent.pojo.Dish;
import com.agent.pojo.DishRel;

@Repository
public class DishDAO {
	
	@Autowired
	private DishMapper dishMapper;
	
	@Autowired
	private DishRelMapper dishRelMapper;
	
	
	public int insertDish(Dish dish) {
		
		int result = dishMapper.insertDish(dish);
		
		return result;
	}
	
	public int insertDishRel(List<DishRel> dishRelList) {
		
		int result = dishRelMapper.insertDishRel(dishRelList);
		
		return result;
	}
	

	public int updateDish(Dish Dish) {
		
		int result = dishMapper.updateDish(Dish);
		
		return result;
	}
	
	public int updateDishRel(DishRel dishRel) {
		
		int result = dishRelMapper.updateDishRel(dishRel);
		
		return result;
	}
	
	public List<Dish> listDish(Dish dish){
		
		List<Dish> listDish = dishMapper.listDish(dish);
		
		return listDish;
	}
	
}
