package com.agent.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agent.dao.mapper.UserMapper;
import com.agent.pojo.User;

@Repository
public class UserDAO {
	
	@Autowired
	private UserMapper userMapper;
	
	
	
	public int insertUser(User user) {
		
		int result = userMapper.insertUser(user);
		
		return result;
	}
	
	public int updateUser(User user) {
		
		int result = userMapper.updateUser(user);
		
		return result;
	}
	
	public User getUser(User user) {
		
		User result = userMapper.getUser(user);
		
		return result;
	}
	
	public List<User> getUserList(User user) {
		
		List<User> result = userMapper.getUserList(user);
		
		return result;
	}


	
}
