package com.agent.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agent.dao.mapper.OrderDetailMapper;
import com.agent.dao.mapper.OrderMapper;
import com.agent.pojo.Order;

@Repository
public class OrderDAO {

	@Autowired
	private OrderMapper orderMapper;
	
	@Autowired
	private OrderDetailMapper orderDetailMapper;
	
	
	public int insertOrder(Order order) {
		
		int result = orderMapper.insertOrder(order);
		
		return result;
	}
	
	public int updateOrder(Order order) {
		
		int result = orderMapper.updateOrder(order);
		
		return result;
	}
	
	public Order getOrder(Order param) {
		
		Order order = orderMapper.getOrder(param);
		
		return order;
	}
	
	public List<Order> getOrderList(Order param) {
		
		List<Order> orderList = orderMapper.getOrderList(param);
		
		return orderList;
	}
	
}
