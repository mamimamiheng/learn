package com.agent.service;

import java.util.List;

import com.agent.pojo.Order;
import com.agent.pojo.OrderDetail;
import com.agent.pojo.Result;

public interface OrderService {
	
	/**
	 * ��Ӷ���
	 */
	public Result addOrder(Order order,List<OrderDetail> orderDetailList);
	
		
	/**
	 * ���Ķ���
	 */
	public Result updateOrder(Order order);
	
	
	/**
	 *查询订单
	 */
	public Order getOrder(Order param);
	
	
	/**
	 *查询订单列表
	 */
	public List<Order> getOrderList(Order order);

}
