package com.agent.service;

import java.util.List;

import com.agent.pojo.Result;
import com.agent.pojo.User;

public interface UserService {
	
	/**
	 * 添加用户
	 */
	public Result addUser(User user);
	
	
	/**
	 * 查询单个用户
	 */
	public User getUser(User user);
	
	
	/**
	 * 查询用户列表
	 */
	public List<User> getUserList(User user);
	
	
	/**
	 * 判断用户是否存在
	 */
	public boolean isUserExists(User user);
	
	
	/**
	 * 更新用户信息
	 */
	public Result updateUser(User user);

}
