package com.agent.service;

import com.agent.pojo.Result;
import com.agent.pojo.User;

public interface LoginService {
	
	public String getValidateCode();
	
	
	public Result userLogin(User param);
	
	
	public Result adminLogin(User param);
	
}
