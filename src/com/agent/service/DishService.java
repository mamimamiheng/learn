package com.agent.service;

import java.util.List;

import com.agent.pojo.Dish;
import com.agent.pojo.DishRel;
import com.agent.pojo.Result;

public interface DishService {
	
	
	public Result addDish(Dish dish,List<DishRel> dishRelList);
	
	
	public Result deleteDish(Dish dish);
	
	
	public Result updateDish(Dish dish,DishRel dishrel);
	
	
	public Result getDish(Dish dish);
	
	
	public List<Dish> getDishList(Dish dish);

}
