package com.agent.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agent.dao.UserDAO;
import com.agent.pojo.Result;
import com.agent.pojo.User;
import com.agent.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO userDAO;
	

	@Override
	public Result<User> addUser(User user) {
		
		Result<User> result = new Result<User>();
		
		int cnt = userDAO.insertUser(user);
		if(1 == cnt) {
			result.setReturnCode("0000");
		}else {
			result.setReturnCode("9999");
		}
		
		return result;
	}
	
	@Override
	public Result<User> updateUser(User user) {
		
		Result<User> result = new Result<User>();
		
		int cnt = userDAO.updateUser(user);
		if(1 == cnt) {
			result.setReturnCode("0000");
		}else {
			result.setReturnCode("9999");
		}
		
		return result;
	}
	

	@Override
	public User getUser(User param) {
		
		User user = userDAO.getUser(param);
		
		return user;
	}
	
	@Override
	public List<User> getUserList(User user) {
		
		List<User> userList = userDAO.getUserList(user);
		
		return userList;
	}
	
	@Override
	public boolean isUserExists(User param) {
		
		boolean isUserExists = true;
		User user = userDAO.getUser(param);
		if(null == user) {
			isUserExists = false;
		}
		
		return isUserExists;
	}

}
