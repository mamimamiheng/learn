package com.agent.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agent.dao.OrderDAO;
import com.agent.pojo.Order;
import com.agent.pojo.OrderDetail;
import com.agent.pojo.Result;
import com.agent.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	private OrderDAO orderDAO;
	

	@Override
	public Result addOrder(Order order,List<OrderDetail> orderDetailList) {
		
        Result result = new Result();
		
		int orderCnt = orderDAO.insertOrder(order);
		if(1 == orderCnt) {
			result.setReturnCode("0000");
		}else {
			result.setReturnCode("9999");
		}
		
//		int orderDetailCnt = orderDAO.insertOrderDetail(orderDetailList);
//		if(1 == orderDetailCnt) {
//			result.setReturnCode("0000");
//		}else {
//			result.setReturnCode("9999");
//		}
		
		return result;
	}

	@Override
	public Result updateOrder(Order order) {
		
        Result result = new Result();
		
		int cnt = orderDAO.insertOrder(order);
		if(1 == cnt) {
			result.setReturnCode("0000");
		}else {
			result.setReturnCode("9999");
		}
		
		return result;
	}

	@Override
	public Order getOrder(Order param) {
		Order order = orderDAO.getOrder(param);
		return order;
	}

	@Override
	public List<Order> getOrderList(Order order) {
		List<Order> orderList = orderDAO.getOrderList(order);
		return orderList;
	}


}
