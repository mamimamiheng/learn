package com.agent.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agent.dao.DishDAO;
import com.agent.pojo.Dish;
import com.agent.pojo.DishRel;
import com.agent.pojo.Result;
import com.agent.service.DishService;

@Service
public class DishServiceImpl implements DishService {
	
	
	@Autowired
	private DishDAO dsishDAO;

	@Override
	public Result addDish(Dish dish,List<DishRel> dishRelList) {
		
		Result result = new Result();
		
		int resDish = dsishDAO.insertDish(dish);
		int resDishRel = dsishDAO.insertDishRel(dishRelList);
		
		return result;
	}

	@Override
	public Result deleteDish(Dish dish) {
		
		Result result = new Result();
		
		int resDish = dsishDAO.updateDish(dish);
//		int resDishRel = dsishDAO.updateDishRel(dishRel);
		
		return result;
	}

	@Override
	public Result updateDish(Dish dish,DishRel dishRel) {

		Result result = new Result();
		
		int resDish = dsishDAO.updateDish(dish);
		int resDishRel = dsishDAO.updateDishRel(dishRel);
		
		return result;
	}

	@Override
	public Result getDish(Dish dish) {

		Result result = new Result();
		
		int resDish = dsishDAO.updateDish(dish);
//		int resDishRel = dsishDAO.updateDishRel(dishRel);
		
		return result;
	}


	@Override
	public List<Dish> getDishList(Dish dish) {
		
		List<Dish> listDish = dsishDAO.listDish(dish);
		
		return listDish;
	}

}
