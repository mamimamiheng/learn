package com.agent.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agent.dao.UserDAO;
import com.agent.pojo.Result;
import com.agent.pojo.User;
import com.agent.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {
	
	@Autowired
	private UserDAO userDAO;
	
	@Override
	public String getValidateCode() {
		
		int randomInt = (int)(Math.random()*9+1)*100000;
		String validateCode = String.valueOf(randomInt);
		
		return validateCode;
		
	}

	@Override
	public Result userLogin(User param) {
		
		User user = userDAO.getUser(param);
		
		Result result = new Result();
		if(null != user) {
			result.setReturnCode("0000");
		}else {
			result.setReturnCode("9999");
		}
		
		return result;
	}

	@Override
	public Result adminLogin(User param) {

		User user = userDAO.getUser(param);
		
		Result result = new Result();
		if(null != user) {
			result.setReturnCode("0000");
		}else {
			result.setReturnCode("9999");
		}
		
		return result;
	}

}
