package com.agent.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.agent.pojo.Order;
import com.agent.pojo.OrderDetail;
import com.agent.pojo.Result;
import com.agent.pojo.User;
import com.agent.service.OrderService;

@Controller
@RequestMapping("orderController")
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	/**
	 * 添加订单
	 * @param order  orderDetail
	 */
	@RequestMapping(value="/addOrder",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result addOrder(@RequestBody Order order,@RequestBody List<OrderDetail> orderDetailList) {
		
		Result result = orderService.addOrder(order,orderDetailList);
		
		return result;
		
	}
	
	/**
	 * 修改订单
	 * @param order  orderDetail
	 */
	@RequestMapping(value="/updateOrder",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result updateOrder(@RequestBody Order order,@RequestBody List<OrderDetail> orderDetailList) {
		
		Result result = orderService.addOrder(order,orderDetailList);
		
		return result;
		
	}
	
	/**
	 * 查询订单列表
	 * @param order  orderDetail
	 */
	@RequestMapping(value="/getOrder",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result<Order> getOrder(@RequestBody Order param) {
		
		Order order = orderService.getOrder(param);
		
		Result<Order> result = new Result<Order>();
//		result.setData(result);
		result.setTotal(1);
		return null;
		
	}
	
	/**
	 * 查询订单列表
	 * @param order  orderDetail
	 */
	@RequestMapping(value="/listOrder",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result<Order> listOrder(Order order) {
		
		List<Order> orderRows = orderService.getOrderList(order);
		
		Result<Order> result = new Result<Order>();
		result.setRows(orderRows);
		result.setTotal(1);
		
		return result;
		
	}
	

}
