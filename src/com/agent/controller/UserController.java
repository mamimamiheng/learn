package com.agent.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.agent.pojo.Result;
import com.agent.pojo.User;
import com.agent.service.LoginService;
import com.agent.service.UserService;
import com.alibaba.fastjson.JSONObject;

@Controller
@RequestMapping("userController")
public class UserController {
	
	private static Logger logger=Logger.getLogger(UserController.class);
	
	@Autowired
	private LoginService loginService;
	
	@Autowired
	private UserService userService;
	
	
	/**
	 * 获取验证码
	 * @param serv_number  validate_code
	 */
	@RequestMapping(value="/getValidateCode",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result<User> getValidateCode(@RequestBody User user) {
		logger.info("=========================>");
		// 生成验证码
		String validateCode = loginService.getValidateCode();
		
		user.setValidateCode(validateCode);
		
		// 根据手机号判断用户是否存在
		boolean isUserExists = userService.isUserExists(user);
		
		Result<User> result;
		// 用户存在时更新用户表中的validate_code  last_login_time
		if(isUserExists) {
			result = userService.updateUser(user);
			
		// 用户不存在时插入一条新的数据
		}else {
			result = userService.addUser(user);
		}
		result.setObject(validateCode);
		
		return result;
		
	}
	
	
	/**
	 * 普通用户登录
	 * @param serv_number  validate_code
	 */
	@RequestMapping(value="/userLogin",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result<User> userLogin(@RequestBody User param) {
		
		String servNumber = param.getServNumber();
		String validateCode = param.getValidateCode();
		
		User user = userService.getUser(param);
		
		Result<User> result = new Result<User>();
        if(servNumber.equals(user.getServNumber()) && validateCode.equals(user.getValidateCode()) ){
        	result.setReturnCode("0000");
		}else{
			result.setReturnCode("9999");
		}
		
		return result;
		
	}
	
	
	/**
	 * 后台用户登录
	 * @param serv_number  validate_code
	 */
	@RequestMapping(value="/adminLogin",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result adminLogin(User param) {
		
		String servNumber = param.getServNumber();
		String validateCode = param.getValidateCode();
		String userType = param.getUserType();
		
		User user = userService.getUser(param);
		
		Result result = new Result();
        if(servNumber.equals(user.getServNumber()) && validateCode.equals(user.getValidateCode()) 
        		&& userType.equals(user.getUserType())){
        	result.setReturnCode("0000");
		}
		
		return result;
		
	}
	
	
	/**
	 * 查询用户信息
	 * @param serv_number
	 */
	@RequestMapping(value="/listUser",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result<User> listUser(User param) {
		
		List<User> userrows = userService.getUserList(param);
		
		Result<User> result = new Result<User>();
		result.setRows(userrows);
		result.setTotal(1);
		return result;
		
	}

}
