package com.agent.controller;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.agent.pojo.User;

@Controller
@RequestMapping("loginController")
public class LoginController {
	
	private static Logger logger=Logger.getLogger(LoginController.class);
	
	@RequestMapping(value="/login",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public String login(@RequestBody User user){
		
		System.out.println(user.getUserId());
		return "{\"userId\":\"11111111\"}";
	}
	

}
