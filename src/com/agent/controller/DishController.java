package com.agent.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.agent.pojo.Dish;
import com.agent.pojo.DishRel;
import com.agent.pojo.Result;
import com.agent.service.DishService;

@Controller
@RequestMapping("dishController")
public class DishController {
	
	private static Logger logger=Logger.getLogger(DishController.class);
	
	@Autowired
	private DishService dishService;
	
	/**
	 * 添加菜品
	 * @param dish  dishrel
	 */
	@RequestMapping(value="/addDish",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result addDish(@RequestBody Dish dish,@RequestBody List<DishRel> dishRelList) {
		
		Result result = dishService.addDish(dish,dishRelList);
		
		return result;
		
	}
	
	/**
	 * 修改菜品
	 * @param dish  dishrel
	 */
	@RequestMapping(value="/updateDish",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result updateDish(@RequestBody Dish dish,@RequestParam("dishrel")DishRel dishrel) {
		
		Result result = dishService.updateDish(dish,dishrel);
		
		return result;
		
	}
	
	/**
	 * 删除菜品
	 * @param dish  dishrel
	 */
	@RequestMapping(value="/deleteDish",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result deleteDish(@RequestBody Dish dish ){
		
		Result result = dishService.deleteDish(dish);
		
		return result;
		
	}
	
	/**
	 * 查询单个菜品
	 * @param dish  dishrel
	 */
	@RequestMapping(value="/getDish",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result getDish(@RequestBody Dish dish ){
		
		Result result = dishService.getDish(dish);
		
		return result;
		
	}
	
	/**
	 * 查询菜品列表
	 * @param dish  dishrel
	 */
	@RequestMapping(value="/listDish",method = RequestMethod.POST,consumes="application/json",produces="application/json")
	@ResponseBody
	public Result<Dish> listDish(Dish param){
		
		List<Dish> dishRows = dishService.getDishList(param);
		
		Result<Dish> result = new Result<Dish>();
		result.setRows(dishRows);
		result.setTotal(1);
		
		return result;
		
	}

}
